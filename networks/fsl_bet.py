# Copyright 2011-2014 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import fastr


def create_network():
    network = fastr.create_network(id="fsl_bet")

    # Source images
    source_image = network.create_source('DicomImageFile', 
                                         id='input',
                                         step_id='input')

    # Parameter sweep
    fraction_threshold_values = {
        "0_20": 0.20,
        "0_35": 0.35,
        "0_50": 0.50,
        "0_65": 0.65,
        "0_80": 0.80,
    }

    fraction_gradient_values = {
        "-0_20": -0.20,
        "-0_10": -0.10,
        "0_00": 0.00,
        "0_10": 0.10,
        "0_20": 0.20,
    }
    fraction_threshold = network.create_constant('Number', fraction_threshold_values,
                                          node_group='fraction_threshold',
                                          id='fraction_threshold',
                                          step_id='brain_extraction')
    
    fraction_gradient = network.create_constant('Number', fraction_gradient_values,
                                          node_group='fraction_gradient',
                                          id='fraction_gradient',
                                          step_id='brain_extraction')

    # Dicom conversions
    image_dicom_to_nifti = network.create_node('dcm2nii/DicomToNifti:0.1', 
                                               id='image_dicom_to_nifti', 
                                               tool_version ='0.1',
                                               step_id='input')
    image_dicom_to_nifti.inputs['dicom_image'] = source_image.output
    
    # use fsl-bet for Automated brain extraction
    brain_extraction = network.create_node('fsl/Bet2:5.0.2', 
                                           tool_version="0.2", 
                                           id='brain_extraction',
                                           step_id='brain_extraction')

    brain_extraction.inputs['image'] = image_dicom_to_nifti.outputs['image']
    brain_extraction.inputs['fraction_threshold'] = fraction_threshold.output
    brain_extraction.inputs['fraction_threshold'].input_group = 'fraction_threshold'

    brain_extraction.inputs['fraction_gradient'] = fraction_gradient.output
    brain_extraction.inputs['fraction_gradient'].input_group = 'fraction_gradient'

    # use pxcast convert to cast masks to char
    cast_brainmask = network.create_node('itktools/PxCastConvert:0.3.0',
                                         tool_version="0.1",
                                         id='cast_brainmask')
    cast_brainmask.inputs['image'] = brain_extraction.outputs['mask']
    cast_brainmask.inputs['component_type'] = 'unsigned_char',

    # Combine bet masks
    combine_masks = network.create_node('itktools/PxCombineSegmentations:0.3.0',
                                        tool_version="0.1",
                                        id='combine_masks',
                                        step_id='output')

    combine_mask_link = network.create_link(cast_brainmask.outputs['image'], combine_masks.inputs['images'])
    combine_mask_link.collapse = ['fraction_threshold', 'fraction_gradient']
    combine_masks.inputs['method'] = 'VOTE',
    combine_masks.inputs['number_of_classes'] = 2,

    # # Save extracted brain images with a sink
    # brain_extract = network.create_sink('NiftiImageFileCompressed', id='brain_extract')
    # brain_extract.inputs['input'] = brain_extraction.outputs['brain']

    # Save brain mask
    brain_mask = network.create_sink('NiftiImageFileCompressed', id='brain_mask', step_id='output')
    brain_mask.inputs['input'] = combine_masks.outputs['hard_segment']
    
    return network


if __name__ == "__main__":
    network = create_network()
    sources = {
        'input': {
            'anonymiz': 'xnat+http://xnat:8080/data/projects/sandbox/subjects/ANONYMIZ001/experiments/ANONYMIZ001_BRAIN/scans/3/resources/DICOM',
        }
    }
    sinks = {
        'brain_mask': 'vfs://home/test-run-output/{sample_id}/nifti/t1w/{sample_id}_mask{ext}',
        'brain_extract': 'vfs://home/test-run-output/{sample_id}/nifti/t1w/{sample_id}_brain{ext}'
    }
    network.execute(sources, sinks, tmpdir='/root/test-run')
